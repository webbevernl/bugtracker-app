export enum STATUS {
    'Logged' = 1,
    'Recreated',
    'In Progresss',
    'Fixed',
    'Declined'
}

export enum SEVERITY {
    'Severe' = 1,
    'Medium',
    'Low',
    'Cosmetic'
}